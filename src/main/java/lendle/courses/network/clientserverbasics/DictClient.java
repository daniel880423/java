/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.zip.InflaterInputStream;

/**
 *
 * @author lendle
 */
public class DictClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Socket socket = new Socket("dict.org", 2628);
        OutputStream outputStream=socket.getOutputStream();
        OutputStreamWriter writer=new OutputStreamWriter(outputStream,"utf-8");
        writer.write("define wn gold\r\n");  // \r\n = enter送出
        writer.flush();  //在OutputStreamWrite下需要緩衝區 (沖馬桶)
        
        InputStream inputStream=socket.getInputStream();
        StringBuilder sb=new StringBuilder();
        InputStreamReader inputStreamReader=new InputStreamReader(inputStream,"utf-8");
        BufferedReader reader=new BufferedReader(inputStreamReader);     //Reader下只能接Reader
        while(true){
            String str=reader.readLine();
            if(".".equals(str)){         //equals比較兩個事件是否相等
                break;
            }
            sb.append(str);
            sb.append("\r\n");
        }

        
//        OutputStream out = socket.getOutputStream();  //取得輸出串流
//        Writer writer = new OutputStreamWriter(out, "UTF-8");   //(輸出串流,編碼)
//        writer.write("DEFINE wn network\r\n");
//        writer.flush();
//        InputStream in = socket.getInputStream();
//        BufferedReader reader = new BufferedReader(     //bufferedreader一次讀一行回傳
//                new InputStreamReader(in, "UTF-8"));
//        for (String line = reader.readLine(); !line.equals("."); line = reader.readLine()) {
//            System.out.println(line);
//        }
//        writer.write("quit\r\n");
//        writer.flush();
        
        System.err.println(sb);
        writer.write("quit\r\n");  // \r\n = enter送出
        writer.flush();  //在OutputStreamWrite下需要緩衝區 (沖馬桶)
        socket.close();               //強制關閉連線
    }

}
